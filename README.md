# find local maximas
Example to find local maximas with scipy

## To use
using powershell

```sh
#create virtual environment
python3 -m venv .env
# enter virtual environment
.env\Scipts\activate.ps1
# install required packages
pip install -r reqs.txt

# run!
python find_local_maximas.py
```

> Note you may need to edit your execution policy `Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser`
