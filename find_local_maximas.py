import numpy as np
import numpy.typing as npt

import matplotlib.pyplot as plt

from scipy.datasets import electrocardiogram
from scipy.signal import find_peaks

if __name__ == "__main__":

    # replace these two lines with your data 
    # this is an ecg signal recorded at 360 Hz
    signal = electrocardiogram()[:2500]
    time = np.arange(signal.size) / 360
    
    # to ignore the tiny peaks, I set an approximate distance between peaks in samples
    peaks, properties = find_peaks(signal, distance=125)

    plt.plot(time,signal)
    plt.xlabel("time (s)")
    plt.ylabel("signal (mV)")

    plt.plot(time[peaks], signal[peaks], "x")

    time_between_peaks = time[peaks[1:]] - time[peaks[:-1]]
    print("time between each peak")
    print(time_between_peaks)
    print()
    print(f"mean= {np.mean(time_between_peaks):6f} seconds\nmedian= {np.median(time_between_peaks):6f} seconds")

    plt.show()

